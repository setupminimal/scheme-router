/// This crate is used to implement several proc
/// macros for `scheme-router`. Refer to the
/// documentation there for examples of their
/// use. You should not need to depend on
/// this crate directly.
extern crate proc_macro;

use lazy_static::lazy_static;
use proc_macro::{TokenStream, TokenTree};
use quote::{format_ident, quote};
use std::sync::Mutex;
use syn::{parse, parse_macro_input, DeriveInput, Ident, ItemFn};

/// We parse the arguments to `#[route()]` into
/// one of these.
type PathInfo = Vec<PathElement>;

/// Capture groups are {}, and match any
/// string that parses into the appropriate
/// argument type.
#[derive(Clone)]
enum PathElement {
    Match(String),
    Capture,
}

lazy_static! {
    /// All registered routes. The string is the
    /// name of the function that is the handler.
    ///
    /// I would have used `Ident`, but those aren't
    /// thread safe.
    static ref ROUTES: Mutex<Vec<(PathInfo, String)>> = Mutex::new(Vec::new());
}

/// Records a given name as a handler.
fn push_handler(path: PathInfo, name: String) {
    ROUTES.lock().unwrap().push((path, name))
}

/// Parses the argument to `#[route]`
fn parse_path(attr: TokenStream, name: String) -> PathInfo {
    let mut path = Vec::new();

    let mut st = attr.into_iter();

    while let Some(elem) = st.next() {
        match elem {
            TokenTree::Punct(p) => {
                if p.as_char() != '/' {
                    panic!("Error while parsing path for {}: Expecting '/'", name);
                }
            }
            _ => panic!("Error while parsing path for {}: Expecting '/'", name),
        }

        path.push(match st.next() {
            Some(TokenTree::Ident(i)) => PathElement::Match(i.to_string()),
            // Technically this allows elements between the braces.
            // I think that's fine? Might be handy for documentation.
            Some(TokenTree::Group(_)) => PathElement::Capture,
            _ => panic!(
                "Error while parsing path for {}: Expecting literal string or '{{}}'.",
                name
            ),
        });
    }

    path
}

/// This validates that a `fn` is declared according
/// to our expectations.
fn validate_ast(ast: &ItemFn) {
    let sig = &ast.sig;
    let name = &sig.ident;

    // `async` has a special meaning that we
    // won't obey - probably best to avoid confusion.
    if sig.asyncness.is_some() {
        panic!("Route handler {} should not be marked `async`.", name);
    }

    // Likewise, we don't really 'return' anything.
    // We could make this required to be Result<Option<usize>>,
    // but I feel like that would be less clear.
    match sig.output {
        syn::ReturnType::Default => (),
        _ => {
            panic!("Route handler {} should not declare a return type.", name);
        }
    }
}

/// `route` is a proc macro that is used to register
/// a handler. See the `scheme-router` documentation
/// for usage.
#[proc_macro_attribute]
pub fn route(attr: TokenStream, input: TokenStream) -> TokenStream {
    let ast: ItemFn = match parse(input) {
        Ok(ast) => ast,
        Err(perr) => panic!("While parsing a route handler: {}", perr),
    };

    validate_ast(&ast);

    let name = &ast.sig.ident;
    let args = &ast.sig.inputs;
    let body = &ast.block;

    push_handler(parse_path(attr, name.to_string()), name.to_string());

    let expanded = quote! {
        fn #name(#args) -> std::pin::Pin<Box<dyn std::ops::Generator<
                scheme_router::Operation,
                // Yielding just responds to a request
                Yield=syscall::Result<Option<usize>>,
                // Returning does so, and then closes the file
                // Further requests will get EBADF
                Return=syscall::Result<Option<usize>>>>> {

            let mut generator =
                // Using `scheme_router_current_operation__`
                // as a capturing variable isn't the best
                // macro hygine, but since the body of the
                // function probably requires match_request!
                // to be functional, and we don't have an
                // easy way to communicate an identifier
                // between those two places, it's what
                // we're going with.
                |mut scheme_router_current_operation__:
                    scheme_router::Operation| {
                #body
                ;
            };

            // This use of unsafe is okay, because Box
            // does not move the contained data; see the
            // rust nightly documentation on Generators.
            //
            // This may at some point be made safe by the
            // stabilization of generators, which will
            // presumably come with some kind of method for
            // creating them in a safe way.
            unsafe { std::pin::Pin::new_unchecked(Box::new(generator)) }
        }
    };

    expanded.into()
}

/// This proc macro makes it easier to handle the implementation
/// of handlers. See `scheme-router` documentation for usage
/// information.
#[proc_macro]
pub fn match_request(input: TokenStream) -> TokenStream {
    let input: proc_macro2::TokenStream = input.into();
    let expanded = quote! {

        // See implementation for `route` for note about
        // this macro un-hygenicness
        yield match scheme_router_current_operation__ {
            #input
        };

    };

    expanded.into()
}

/// This proc macro automatically derives an
/// implementation for `RouterScheme`. See `scheme-router`
/// for usage and why you would want this.
#[proc_macro_derive(RouterScheme)]
pub fn router_scheme(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    // We need a type to hold the information about our
    // currently open file handlers, and we can't use the
    // type that this annotates, because derive macros
    // aren't allowed to re-write the structures that
    // they decorate.
    //
    // Therefore we use a name that (hopefully) won't
    // collide with anything in the client program.
    let name = format_ident!("{}ResultSchemeInner", &input.ident);
    let outer_name = &input.ident;

    // Process the registered routes to generate the
    // fragment that matches against the registered
    // paths.
    let routes = ROUTES
        .lock()
        .unwrap()
        .clone()
        .into_iter()
        .map(|(pinfo, name)| {
            // `name` needs to be made into an Ident so that
            // when it is spliced into a quote, it comes out
            // as a function name and not a string literal.
            let name = Ident::new(&name, outer_name.span());

            // We're going to count the capture groups that
            // we encounter, so that we can generate appropriate
            // variables.
            let mut arg_cnt = 0;

            let parts: Vec<_> = {
                pinfo
                    .into_iter()
                    .map(|part| match part {
                        PathElement::Capture => {
                            let argid = Ident::new(&format!("arg_{}", arg_cnt), input.ident.span());
                            arg_cnt += 1;
                            // Capture groups assign their contents
                            // to a fresh variable.
                            quote! {
                                let f = parts.next();
                                if f.is_none() {
                                    break;
                                }
                                let #argid = f.unwrap().from_str();
                            }
                        }
                        // Non-capture groups either match
                        // or don't.
                        PathElement::Match(st) => quote! {
                            let f = parts.next();
                            if f.is_none() {
                                break;
                            }
                            let part = f.unwrap();
                            if #st != *part {
                                break;
                            }
                        },
                    })
                    .collect()
            };

            let names = (0..arg_cnt).map(|i| Ident::new(&format!("arg_{}", i), input.ident.span()));

            // We use a `while true` loop here so that
            // we can use `break` to skip to the end
            // later. (See above)
            quote! {
                while true {

                    let mut parts = pelem.iter();

                    #(
                      #parts
                    )*

                    let handler = #name(
                        #(#names),*
                    );

                    self.handles.insert(id, handler);

                    return Ok(Some(id));
                }
            }
        });

    let expanded = quote! {
        struct #name {
            // Used to hand out unique file ids
            next_id: usize,

            // Used to record handlers for open files
            handles: std::collections::BTreeMap<usize,
                              std::pin::Pin<Box<dyn std::ops::Generator<
                                      scheme_router::Operation,
                                  Yield=syscall::Result<Option<usize>>,
                                  Return = syscall::Result<Option<usize>>>>>>
        }

        impl syscall::SchemeBlockMut for #name {
            fn open(&mut self, path: &[u8], flags: usize, uid: u32,
                    gid: u32) -> syscall::error::Result<Option<usize>> {
                self.next_id += 1;
                let id = self.next_id;

                let pstr = String::from_utf8(path.to_owned())
                    .map_err(|_| syscall::error::Error::new(syscall::error::ENOENT))?;
                let pelem: Vec<&str> = pstr.split("/").collect();

                #(
                    #routes
                )*

                Err(syscall::error::Error::new(syscall::error::ENOENT))
            }

            fn read(&mut self, id: usize, buf: &mut [u8]) -> syscall::error::Result<Option<usize>> {
                let handle = self.handles.get_mut(&id).ok_or(syscall::error::Error::new(syscall::error::EBADF))?;

                let rc = std::rc::Rc::new(std::cell::RefCell::new(buf.to_owned()));
                let ours = rc.clone();

                if let Some(len) = match std::ops::Generator::<_>::resume(handle.as_mut(), scheme_router::Operation::Read(rc)) {
                    std::ops::GeneratorState::Yielded(u) => u,
                    std::ops::GeneratorState::Complete(u) => {
                        self.handles.remove(&id).unwrap();
                        u
                    },
                }? {
                    // We, unfortunately, need this copy because
                    // there is no guarantee whether the handler
                    // keeps a reference to whatever we send it,
                    // so we can't just directly send `buf` or a
                    // reference to it.
                    //
                    // If you have a clever way around this,
                    // please do let me know.
                    //
                    // `copy_from_slice` complies down to a `memcpy`,
                    // which is really fast on modern computers,
                    // so this shouldn't be _too_ bad.
                    buf.copy_from_slice(&(ours.borrow()[0..len]));

                    Ok(Some(len))
                } else {
                    Ok(None)
                }
            }

            fn write(&mut self, id: usize, buf: &[u8]) -> syscall::error::Result<Option<usize>> {
                let handle = self.handles.get_mut(&id).ok_or(syscall::error::Error::new(syscall::error::EBADF))?;

                let rc : std::rc::Rc<Vec<u8>> = std::rc::Rc::new(buf.to_owned());

                match std::ops::Generator::<_>::resume(handle.as_mut(), scheme_router::Operation::Write(rc)) {
                    std::ops::GeneratorState::Yielded(u) => u,
                    std::ops::GeneratorState::Complete(u) => u,
                }
            }

            fn close(&mut self, id: usize) -> syscall::error::Result<Option<usize>> {
                let _ = self.handles.remove(&id).ok_or(syscall::error::Error::new(syscall::error::EBADF))?;
                Ok(Some(0))
            }

            fn fsync(&mut self, id: usize) -> syscall::error::Result<Option<usize>> {
                let handle = self.handles.get_mut(&id).ok_or(syscall::error::Error::new(syscall::error::EBADF))?;

                match std::ops::Generator::<_>::resume(handle.as_mut(), scheme_router::Operation::FSync) {
                    std::ops::GeneratorState::Yielded(u) => u,
                    std::ops::GeneratorState::Complete(u) => u,
                }
            }
        }

        impl scheme_router::RouterScheme for #outer_name {
            fn scheme() -> Box<dyn syscall::SchemeBlockMut> {
                Box::new(#name {
                    next_id: 0,
                    handles: std::collections::BTreeMap::new()
                })
            }

            fn start(name: &str) -> std::io::Result<!> {
                Self::start_with_callback(name, || {
                    syscall::setrens(0, 0).map_err(|e| std::io::Error::from_raw_os_error(e.errno))?;
                    Ok(())
                })
            }

            fn start_with_callback<F: FnOnce() -> std::io::Result<()>>(name: &str, f: F) -> std::io::Result<!> {
                use syscall::{Packet, Event, EVENT_READ, O_NONBLOCK};
                use std::fs::OpenOptions;
                use std::os::unix::fs::OpenOptionsExt;
                use std::os::unix::io::AsRawFd;
                use std::io::{Write, Read};
                use std::collections::VecDeque;

                // Create the scheme handler
                let mut scheme_file = OpenOptions::new()
                    .create(true).read(true).write(true)
                    .custom_flags(O_NONBLOCK as i32)
                    .open(&format!(":{}", name))?;

                // Create the event-handling file
                let mut event_file = OpenOptions::new()
                    .read(true).write(true)
                    .open("event:")?;

                f()?;

                // Listen for events
                const SCHEME_TOKEN: usize = 1;
                event_file.write(&Event {
                    id: scheme_file.as_raw_fd() as usize,
                    flags: EVENT_READ,
                    data: SCHEME_TOKEN,
                })?;

                let mut scheme = Self::scheme();
                let mut blocked = VecDeque::new();

                // Process events as they happen.
                loop {
                    let mut event = Event::default();
                    event_file.read(&mut event)?;

                    match event.data {
                        SCHEME_TOKEN => {
                            let mut next_packet = Packet::default();
                            scheme_file.read(&mut next_packet)?;

                            blocked.push_front(next_packet);
                        },
                        _ => (),
                    }

                    let mut i = 0;
                    while i < blocked.len() {
                        if let Some(response) = scheme.handle(&blocked[i]) {
                            let mut packet = blocked.remove(i).unwrap();
                            packet.a = response;
                            scheme_file.write(&packet)?;
                        } else {
                            i += 1;
                        }
                    }
                }
            }
        }
    };

    expanded.into()
}
