Scheme Router implements a set of convenience functions
and macros that let you write implementations for
custom schemes more easily, like this:

```{rust}
// Used to enable Generators, which are used
// to handle requests.
#![feature(generator_trait)]
#![feature(generators)]

// Used to allow declaring that certain
// associated functions of a trait diverge
#![feature(never_type)]

use scheme_router::*;

#[route(/foo/{}/baz)]
fn foo_handler(_x: u8) {
  // Runs whenever anyone opens a file at
  // the path our_scheme:/foo/<any number>/baz
  println!("Do any custom initialization.");

  loop {
    match_request! {
      Operation::Read(ref v) => {
        // Fill in read bytes

        // Return number of bytes supplied,
        // or indicate that an error occurred
        // or that the operation blocked
        // (Works just like SchemeMutBlock)
        Ok(Some(v.borrow_mut().len()))
      },
      // Other operations include `Write` and `FSync`
      _ => {
        Err(syscall::error::Error::new(syscall::EIO))
      }
    }
  }
}

#[derive(RouterScheme)]
struct Foo;

fn main() {
  Foo::start("example");
}
```

`#[route(/path/with/capture/{}/groups)]` is used to
register a function as a handler. Then the function
is compiled into a generator that is called when
another process opens a file handle.

`#[derive(RouterScheme)]` is used to derive all of
the machinery to create an actual `SchemeBlockMut`,
and then wraps it in the convenience trait `RouterScheme`
that lets you simply `start` all your handlers.
