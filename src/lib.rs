#![feature(never_type)]

/// This module implements a set of convenience functions
/// and macros that let you write implementations for
/// custom schemes more easily, like this:
///
/// ```
/// // Used to enable Generators, which are used
/// // to handle requests.
/// #![feature(generator_trait)]
/// #![feature(generators)]
///
/// // Used to allow declaring that certain
/// // associated functions of a trait diverge
/// #![feature(never_type)]
///
/// use scheme_router::*;
///
/// #[route(/foo/{}/baz)]
/// fn foo_handler(_x: u8) {
///   // Runs whenever anyone opens a file at
///   // the path our_scheme:/foo/<any number>/baz
///   println!("Do any custom initialization.");
///
///   loop {
///     match_request! {
///       Operation::Read(ref v) => {
///         // Fill in read bytes
///
///         // Return number of bytes supplied,
///         // or indicate that an error occurred
///         // or that the operation blocked
///         // (Works just like SchemeMutBlock)
///         Ok(Some(v.borrow_mut().len()))
///       },
///       // Other operations include `Write` and `FSync`
///       _ => {
///         Err(syscall::error::Error::new(syscall::EIO))
///       }
///     }
///   }
/// }
///
/// #[derive(RouterScheme)]
/// struct Foo;
///
/// fn main() {
///   Foo::start("example");
/// }
/// ```
///
/// `#[route(/path/with/capture/{}/groups)]` is used to
/// register a function as a handler. Then the function
/// is compiled into a generator that is called when
/// another process opens a file handle.
///
/// `#[derive(RouterScheme)]` is used to derive all of
/// the machinery to create an actual `SchemeBlockMut`,
/// and then wraps it in the convenience trait `RouterScheme`
/// that lets you simply `start` all your handlers.
pub use scheme_router_derive::*;

use std::cell::RefCell;
use std::rc::Rc;
use syscall::scheme::SchemeBlockMut;

/// An `Operation` encapsulates a read or write
/// operation being applied to a file.
///
/// The buffers
/// encapsulated in `Read` and `Write` are contained
/// in an `Rc` because handlers may hold onto
/// them between invocations. After the handler
/// yields, the buffers will be dropped unless
/// the handler does so.
pub enum Operation {
    Read(Rc<RefCell<Vec<u8>>>),
    Write(Rc<Vec<u8>>),
    FSync,
}

/// `RouterScheme` should be derived like so:
///
/// ```
/// #[derive(RouterScheme)]
/// struct Foo;
/// ```
///
/// Then `Foo::start(name)` can be used
/// to start processing requests using the
/// declared handlers.
pub trait RouterScheme {
    /// Returns the underlying scheme used.
    /// Usually automatically created via
    /// `derive(RouterScheme)`.
    fn scheme() -> Box<dyn SchemeBlockMut>;

    /// `start` begins handling requests
    /// using the declared handlers. It
    /// never returns unless an error is
    /// encountered.
    ///
    /// It will attempt to create the scheme
    /// with the given `name` using the `:`
    /// scheme.
    ///
    /// It will then switch to a namespace
    /// that does not allow opening any
    /// additional files, and then start
    /// handling requests. If your application
    /// requires opening files dynamically,
    /// you should use the `start_with_callback`
    /// function and perform more complicated
    /// privilage dropping logic there.
    fn start(name: &str) -> std::io::Result<!>;

    /// This function performs the same function
    /// as `start`, but instead of dropping
    /// into an empty namespace once everything
    /// is set up, it calls the provided function
    /// to implement any privilage dropping logic
    /// the application requires
    fn start_with_callback<F: FnOnce() -> std::io::Result<()>>(
        name: &str,
        f: F,
    ) -> std::io::Result<!>;
}
